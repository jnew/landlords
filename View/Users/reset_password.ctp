


<section class="loginArea">
	<div class="row">
		<div class="large-12 columns text-center">
			<?php echo $this->Session->flash();?>
		</div>
	</div>
	<div class="row">
		<div class="large-3 loginLogo large-centered columns">
			<?php echo $this->Html->image('logo.png');?>
		</div>
	</div>
	<div class="row">
		<div class="large-8 large-centered columns">
			<?php
				echo $this->Form->create('User', array('action' => 'reset_password'));
				echo $this->Form->input('id', array('value' => $userId, 'type' => 'hidden'));
				echo $this->Form->input('password',array('div'=>false,'Placeholder'=>'(Required)','class' => 'required','label' => 'Password', 'type' => 'password'));
			?>
			<?php echo $this->Form->Submit('SUBMIT',array('class'=>'redButton'));?>
			<?php echo $this->Form->end(); ?>	
		</div>
	</div>
</section>


