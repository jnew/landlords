<div class="adminPage">
	<div class="adminSection">
		<div class="row">
			<div class="large-12 columns text-center">
				<h1 class="adminSectionMainHeading adminPageTitle">Add Group</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-6 large-centered columns">

					<?php
						echo $this->Form->create('Group', array('action' => 'add'));
						echo $this->Form->input('name');
						echo $this->Form->submit('Save',array('div' => false,'class' => 'fadeButton borderRadiusButton greenAdminButton adminButton'));
						echo $this->Form->End();
					?>

			</div>
		</div>
	</div>
</div>



