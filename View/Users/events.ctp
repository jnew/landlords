<?php
	function ordinalSuffix($n){
	  return date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
?>
<?php echo $this->Html->image('write.png'); ?>
<div class="userProfilePage">
	<?php if(empty($userID)):?>
		<h1>Please Login!</h1>
	<?php elseif($approved == false):?>
		<h1>Your account has not yet been approved!</h1>
	<?php else:?>
		<div class="row">
			<div class="large-12 columns">
				<h1>Hello <?php echo ucfirst($user['User']['first_name']).' '.ucfirst($user['User']['last_name']);?>!</h1>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="registeredEvents">
					<h2 class="eventPin">All Events You Have Registered For:</h2>
					<?php foreach($registeredEvents as $registeredEvent):
						$year = substr($registeredEvent['Event']['start'],0,4);
						$month = substr($registeredEvent['Event']['start'],5,2);
						$day = substr($registeredEvent['Event']['start'],8,2);
					?>
						<div class="row subEventRow">
							<div class="large-12 columns">
								<ul class="medium-block-grid-3 upcomingEvents">
									<li>
										<i class="fa fa-chevron-circle-right"></i> <a href="<?php echo $this->webroot;?>events_calendar/events/view/<?php echo $registeredEvent['Event']['id'];?>"><?php echo $registeredEvent['Event']['title'];?></a>
									</li>
									<li>
										<i class="fa fa-clock-o"></i> <?php echo $day; echo ordinalSuffix($day);?> <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?> <?php echo substr($registeredEvent['Event']['start'],11,5); ?> - <?php echo substr($registeredEvent['Event']['end'],11,5); ?>
									</li>
									<li>
										<a class="directions" target="_blank" href="<?php echo $registeredEvent['Event']['directions'];?>"><i class="fa fa-map-marker"></i> GET DIRECTIONS</a> 
										<a href="<?php echo $this->webroot;?>events_calendar/event_registrations/unregister/<?php echo $registeredEvent['Event']['id'];?>"><i class="fa fa-multiple"></i>Unregister <i class="fa fa-times-circle"></i></a> 
									</li>
								</ul>
							</div>
						</div>
					<?php endforeach;?>
				</div>
				<a href="<?php echo $this->webroot;?>users/profile" class="redButton doubleButton">Back To Profile</a>
			</div>
		</div>
		<a href="<?php echo $this->webroot;?>users/logout">Logout</a>
		<a href="<?php echo $this->webroot;?>users/profile">Back</a>
	<?php endif;?>
</div>

