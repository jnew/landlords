<?php
	class LandlordGroup extends AppModel {
	    public $actsAs = array('Acl' => array('type' => 'requester'));
  		public $hasMany = array('Landlord');
	    public function parentNode() {
	        return null;
	    }
	    public $validate = array(
	        'title' => array(
	            'notEmpty' => array(
	                'rule' => 'notEmpty',
	                'message' => 'Title Required!',
	                'required' => true
	            )
	        )
	        
	    );
	}
?>