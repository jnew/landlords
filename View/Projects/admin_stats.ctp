<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle"><?php echo $landlord['Landlord']['title'];?> Stats</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
				<div class="large-12 columns">
					<?php echo $this->Html->link('View Landlords',array('controller' => 'landlords','action' => 'index'),array('class' => 'adminButton floatRight greenAdminButton'));?>
					<?php echo $this->Html->link('View '.ucfirst($landlord['Landlord']['title']).' Projects',array('controller' => 'projects','action' => 'landlord',$landlord['Landlord']['id']),array('class' => 'adminButton floatRight blueAdminButton'));?>
				</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<ul class="large-block-grid-2">
					<li class="costChart">
						<h5>
							 Cost Per Quarter (Thousands) - <span class="quarterYear"><?php echo $years[0];?></span><br />
							<span class="key estimatedKey">
								Estimated
							</span>
							<span class="key actualKey">
							 Actual
							</span>

						</h5>
						<select class="yearDropdownQuarter" data-canvas-id="quarterlyStats">
							<?php foreach($years as $year):?>
								<option value="<?php echo $year;?>"><?php echo $year;?></option>
							<?php endforeach;?>
						</select>
						<div class="canvas-container">
				            <canvas data-set="quarterly" data-type="Bar" data-landlord-id="<?php echo $landlord['Landlord']['id'];?>" id="quarterlyStats" data-year="<?php echo $years[0];?>">
				            </canvas>
				        </div>
					</li>
					<li>
						<h5>
							 Project Loss
						</h5>
						
						<div class="canvas-container">
				            <canvas data-set="loss" data-type="Bar" data-landlord-id="<?php echo $landlord['Landlord']['id'];?>" id="lossStats">
				            </canvas>
				        </div>
					</li>
				</ul>	
			</div>
		</div>
	</section>
</div>