<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo ('Landlord Dashboard -'); ?>
		<?php echo $title_for_layout; ?>

	</title>
	<meta name="viewport" content="width=device-width; initial-scale=1.0;"> 
	<?php
		if( in_array( $_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
			echo '<script src="//localhost:1990/livereload.js"></script>';
		}
		echo $this->Html->meta('icon');
		echo $this->Html->css('styles.css');
	?>
</head>
	<body class="admin">
	<div class="rootURL" style="display:none;"><?php echo $this->Html->url( '/', true );?></div>
		<?php echo $this->Element('Layout/adminHeader');?>
		<?php echo $this->Element('Layout/adminSidebar');?>
		<?php echo $this->fetch('content'); ?>
	</body>
	<?php echo $this->Element('Global/javascript'); ?>


</html>