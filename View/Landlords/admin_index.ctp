<section class="adminSection">
<div class="row">
	<div class="large-12 columns">
		<h1 class="adminSectionHeading adminPageTitle">Landlords</h1>
	</div>
</div>
<div class="row">
		<div class="large-12 columns">
			<?php echo $this->Html->link('Add Landlord',array('controller' => 'landlords','action' => 'add'),array('class' => 'adminButton greenAdminButton'));?>
		</div>
</div>
<div class="row">
	<div class="large-12 columns">
	
	<?php echo $this->Session->flash();?>
	<?php if(!empty($landlords)):?>
		<table class="adminTable postsTable">
			<thead>
				<tr class="blackGradient">
				  <th class="blogTitleCell">Name</th>
				  <th class="blogActionsCell text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($landlords as $landlord):?>
				<tr>
				  <td class="adminTitleCell">
				 	 <?php echo $landlord['Landlord']['title'];?>
				  </td>
				  <td class="adminActionsCell articlesActions text-center">
				  <?php
				  		if(!empty($landlord['Project'])){
				  			$statsMarkup = "<span class='adminIcon fa-stack'>";
							$statsMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
							$statsMarkup .= "<i class='has-tip tip-bottom fa fa-bar-chart fa-stack-1x' data-tooltip title='View Stats'></i>";
							$statsMarkup .= "</span>";

							echo $this->Html->link(
								$statsMarkup,
								array('controller' => 'projects','action' => 'stats' , $landlord['Landlord']['id']),
								array('escape' => false)
							);
				  		}

					?>
				  	<?php echo $this->Element('Global/deleteIcon',array('controllerName' => 'landlords','id' => $landlord['Landlord']['id'],'editItem' => 'landlord'));?>
				  </td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	<?php else:?>
		<h1>No Landlords Found!</h1>
	<?php endif;?>

	</div>
</div>
</section>