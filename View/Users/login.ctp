<section class="appLogin">
	<div class="row">
		<div class="large-12 text-center columns">
			<?php echo $this->Html->image('landlordLogo.png');?>
		</div>
	</div>
	<div class="row">
		<div class="large-5 large-centered columns">
			<?php echo $this->Session->flash();?>
			<?php echo $this->Element('Global/loginForm'); ?>
		</div>
	</div>
</section>