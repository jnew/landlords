<?php
	class Landlord extends AppModel {
	    public $actsAs = array('Acl' => array('type' => 'requester'));
	    public $hasMany = array('Project');
	    public $belongsTo = array('LandlordGroup');
	    public function parentNode() {
	        return null;
	    }
	}
?>