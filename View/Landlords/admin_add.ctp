<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle">Add Landlord</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-8 columns">
					<?php
						echo $this->Form->create('Landlord', array('action' => 'add'));
						echo $this->Form->input('title',array('type' => 'text'));
						echo $this->Form->input('landlord_group_id', array('options' => $groups));
						echo $this->Form->submit('Save',array('div' => false,'class' => 'fadeButton borderRadiusButton greenAdminButton adminButton'));
						echo $this->Form->End();
					?>

			</div>
		</div>
	</section>
</div>