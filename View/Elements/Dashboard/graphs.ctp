<ul class="large-block-grid-2">
	<li>
		<h5>
			 Projects Summary(£GBP) <br />
			<span class="key estimatedKey">
				Estimated Cost 
			</span>
			VS
			<span class="key actualKey">
			 Actual Cost
			</span>
		</h5>
		<div class="canvas-container">
            <canvas data-set="yearlySummary" data-type="Bar" id="yearlyStats"></canvas>
        </div>
	</li>
	<li>
		<h5>
			 Project Profits By Landlords (£GBP) <br />	<br />
		</h5>
		<div class="canvas-container">
            <canvas data-set="performers" data-type="Pie" id="performers"></canvas>
        </div>
	</li>
</ul>