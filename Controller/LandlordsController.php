<?php
	class LandlordsController extends AppController {
		public function beforeFilter() {
		    parent::beforeFilter();
		}

		public function admin_index(){
			$this->layout = 'admin';
			$this->set('landlords',$this->Landlord->find('all'));
		}
		public function admin_add(){
			$this->layout = 'admin';
			if($this->request->is('post')){
				$this->set('groups',$this->Landlord->LandlordGroup->find('list'));
				if($this->Landlord->save($this->request->data)){
					$this->Session->setFlash('Landlord Added!','default',array('class' => 'formSuccess'));
					return $this->redirect(array('action' => 'admin_index'));
				}else{
					$errors = $this->LandlordGroup->validationErrors;
				}
			}else{
				$this->set('groups',$this->Landlord->LandlordGroup->find('list'));
			}
			
		}


		public function admin_delete($id){
			if ($this->Landlord->delete($id)) {
            	$this->Session->setFlash('Landlord Deleted','default',array('class' => 'formError'));
            	$this->redirect(array('action' => 'admin_index'));
        	}
		}
		
		
	}
?>
