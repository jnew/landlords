<?php

	$deleteMarkup = "<span class='adminIcon deleteIcon fa-stack'>";
	$deleteMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
	$deleteMarkup .= "<i class='has-tip tip-bottom fa fa-times fa-stack-1x' data-tooltip title='Delete $editItem'></i>";
	$deleteMarkup .= "</span>";

	echo $this->Html->link(
		$deleteMarkup,
		array('controller' => $controllerName,'action' => 'delete' , $id),
		array('escape' => false),'Are you sure you want to delete this '.strtolower($editItem).'? This action cannot be undone!'
	);
?>