<section class="adminSection">
<div class="row">
		<div class="large-12 columns">
			<h1 class="adminSectionHeading adminPageTitle">Projects</h1>
			<?php echo $this->Session->flash();?>
		</div>
</div>
<div class="row">
		<div class="large-12 columns">
			<?php echo $this->Html->link('Add Project',array('controller' => 'projects','action' => 'add'),array('class' => 'adminButton greenAdminButton'));?>
		</div>
</div>
<div class="row">
	<div class="large-12 columns">

	<?php if(!empty($projects)):?>
		<table class="adminTable postsTable">
			<thead>
				<tr class="blackGradient">
				  <th class="blogTitleCell">Landlord</th>
				  <th class="text-center">Projects Count</th>
				  <th class="blogActionsCell text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($projects as $project):?>
				
					<tr>
					  <td class="adminTitleCell">
					    <?php echo $project['Landlord']['title'];?>
					  </td>
					  <td class="text-center">
					  	<?php echo $project['Project']['count'];?>
					  </td>
					  <td class="adminActionsCell articlesActions text-center">
						<?php
							$statsMarkup = "<span class='adminIcon fa-stack'>";
							$statsMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
							$statsMarkup .= "<i class='has-tip tip-bottom fa fa-bar-chart fa-stack-1x' data-tooltip title='View Stats'></i>";
							$statsMarkup .= "</span>";

							echo $this->Html->link(
								$statsMarkup,
								array('controller' => 'projects','action' => 'stats' , $project['Project']['landlord_id']),
								array('escape' => false)
							);
							$projectsMarkup = "<span class='adminIcon fa-stack'>";
							$projectsMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
							$projectsMarkup .= "<i class='has-tip tip-bottom fa fa-list fa-stack-1x' data-tooltip title='View Projects'></i>";
							$projectsMarkup .= "</span>";

							echo $this->Html->link(
								$projectsMarkup,
								array('controller' => 'projects','action' => 'landlord' , $project['Project']['landlord_id']),
								array('escape' => false)
							);
						?>
					  </td>
					</tr>


			<?php endforeach;?>
			</tbody>
		</table>
	<?php else:?>
		<h1>No Projects Found!</h1>
	<?php endif;?>

	</div>
</div>
</section>