<section class="loginArea">
	<div class="row">
		<div class="large-12 columns text-center">
			<?php echo $this->Session->flash();?>
		</div>
	</div>
	<div class="row">
		<div class="large-3 loginLogo large-centered columns">
			<?php echo $this->Html->image('logo.png');?>
		</div>
	</div>
	<div class="row">
		<div class="large-8 large-centered columns">
			<?php 	
				echo $this->Form->create('User', array('action' => 'forgot_password'));
				echo $this->Form->inputs(array(
				    'legend' => __('Forgot Password'),
				    'email'
				));
				echo $this->Form->submit('Submit',array('class' => ' centerButton redButton'));
				echo $this->Form->end();
			?>
		</div>
	</div>
</section>


