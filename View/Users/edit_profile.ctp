<?php echo $this->Html->image('write.png'); ?>
<h1>Edit Profile</h1>
<div class="row">
	<div class="large-12 columns">
		<?php echo $this->Session->flash();?>
	</div>
</div>


<div class="userProfilePage">
	<?php if(empty($userID)):?>
		<h1>Please Login!</h1>
		<a href="<?php echo $this->webroot;?>users/login">Login</a> OR <a href="<?php echo $this->webroot;?>users/register">Register</a>
	<?php else:?>
		<div class="sawtooth">
		</div>

		<div class="redrow darkerRow profilePicArea">
			<div class="row">
				<div class="large-12 columns">
					<?php echo $this->Form->create('User',array('type' => 'file'));?>
					<?php echo $this->Form->hidden('id',array('value' => $userID));?>
					<?php if(!empty($originalPic)):?>
						<?php echo $this->Html->image('uploads/'.$originalPic,array('class' => 'formProfileThumbnail'));?>
					<?php else:?>
						<?php echo $this->Html->image('userplaceholder.png',array('class' => 'formProfileThumbnail'));?>
					<?php endif;?>
					<?php echo $this->Form->input('picture',array('type' => 'file','label' => 'Edit Profile Image'));?>
				</div>
			</div>
		</div>

		<div class="sawtooth saw3">
		</div>

		<div class="row profileFields">
			<div class="large-6 medium-6 columns">
				<?php echo $this->Form->input('username',array('type' => 'text'));?>
				<?php echo $this->Form->input('first_name',array('type' => 'text'));?>
				<?php echo $this->Form->input('last_name',array('type' => 'text'));?>
				<?php echo $this->Form->input('company',array('type' => 'text'));?>
				
			</div>
			<div class="large-6 medium-6 columns">
				<?php echo $this->Form->input('change_password',array('type' => 'text'));?>
				<?php echo $this->Form->input('confirm_password',array('type' => 'text'));?>
				<?php echo $this->Form->input('phone');?>
				<?php echo $this->Form->input('email',array('type' => 'text'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<?php echo $this->Form->submit('Submit',array('class' => 'redButton doubleButton'));?>
				<a href="<?php echo $this->webroot;?>users/profile" class="redButton doubleButton">Back To Profile</a>
				<?php echo $this->Form->end();?>
			</div>
		</div>
	<?php endif;?>
</div>

