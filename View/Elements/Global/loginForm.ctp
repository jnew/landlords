<?php 	
	echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login','plugin' => false)));

	echo $this->Form->input('username', array('div' => false, 'label' => false, 'placeholder' => 'Username'));
	echo $this->Form->input('password', array('div' => false, 'label' => false, 'placeholder' => 'Password'));

	echo $this->Form->submit('Login',array('class' => 'greenButton fadeButton greenAdminButton adminButton','div' => false));
	echo $this->Form->end();
?>