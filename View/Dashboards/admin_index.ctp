<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle text-center">Landlord Dashboard</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<?php if(!empty($actualUpdate)):?>
			<div class="row">
				<div class="large-12 columns dashboardError">
					<strong><?php echo $actualUpdate;?></strong> Projects Need An Actual Cost Assigned!
				</div>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="large-12 columns">
				<?php echo $this->Element('Dashboard/stats');?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns dashboardGraphs">
				<?php echo $this->Element('Dashboard/graphs');?>
			</div>
		</div>
	</section>
</div>