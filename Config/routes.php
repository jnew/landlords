<?php
App::uses('Folder', 'Utility');
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/dashboard', array('controller' => 'dashboards' , 'action' => 'index','admin' => 'true'));
	Router::connect('/admin/stats/*',array('controller' => 'projects','action' => 'stats','admin' => 'true'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	
	$dir = new Folder('../View/Pages');
	$files = $dir->find('.*\.ctp');
	$staticPages = array();
	foreach($files as $file){
		array_push($staticPages, substr($file, 0, -4));
	}
	$staticList = implode('|', $staticPages);
	
	Router::connect('/:static', array(
	 	'plugin' => false,
	 	'controller' => 'pages',
	 	'action' => 'display'), array(
	 		'static' => $staticList,
	 		'pass' => array('static')
	 		)
	);

	require CAKE . 'Config' . DS . 'routes.php';
