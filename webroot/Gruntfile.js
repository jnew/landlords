module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		bowercopy: {
			options: {
				clean: true
			},
			scss: {
				options: {
					destPrefix: 'src/scss'
				},
				files: {
					// Foundation
					'foundation/': 'foundation/scss/foundation/*.scss',
					//'_settings.scss': 'foundation/scss/foundation/_settings.scss',
					'foundation/components/': 'foundation/scss/foundation/components/*.scss',
					//Slick
					//'slick/_slick.scss': 'slick.js/slick/slick.scss'
				}
			},
			js: {
				options: {
					destPrefix: 'src/js'
				},
				files: {
					// Foundation
					'foundation.js': 'foundation/js/foundation.min.js',
					'jquery.js': 'foundation/js/vendor/jquery.js',
					'fastclick.js': 'foundation/js/vendor/fastclick.js',
					'modernizr.js': 'foundation/js/vendor/modernizr.js',
					//Slick
					'slick.js':'slick.js/slick/slick.min.js'
				}
			}
		},

		sass: {
			dist: {
				options: {
					outputStyle: 'compressed'
				},
				files: {
					'css/styles.css': 	'src/scss/styles.scss'
				}
			}
		},




		//Uglify
		uglify: {
			my_target: {
				files: {
					'js/main.min.js': [
										'src/js/jquery.js',
										'src/js/foundation.js',
										'src/js/charts.js',
										'src/js/main.js',
										'src/js/slick.js',
										'src/js/fastclick.js',
										'src/js/modernizr.js'
		
									]
				}
			}
		},

		//Watch Tasks
		watch: {
			livereload: {
				options: {
					livereload: 1990
				},
				files: ['src/scss/*.scss', '../Controller/*.php','src/*.html', 'src/js/*.js','../View/Pages/*.ctp','../View/Elements/Global/*.ctp','../View/Layouts/*.ctp']
			},

			grunt: {
				files: ['Gruntfile.js']
			},

			js: {
				files: 'src/js/*.js',
				tasks: ['uglify']
			},

			sass: {
				files: 'src/scss/**/*.scss',
				tasks: ['sass']
			}
		}
	});

	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-bowercopy');

	grunt.registerTask('build', ['sass', 'uglify','watch']);
	grunt.registerTask('default', ['build']);

}
