<?php 
	App::uses('CakeEmail', 'Network/Email');
	class UsersController extends AppController {
	
		public function beforeFilter() {
		    parent::beforeFilter();
		    $this->Auth->allow(array('login','logout'));
		}

		public function login() {
			$this->layout = 'login';
		    if ($this->request->is('post')) {
		        if ($this->Auth->login()) {
		        	if($this->Auth->user('group_id') == 1){
		        		$this->User->id = $this->Auth->user('id');
			  			$this->User->save(array('last_login' => date('Y-m-d H:i:s')),array('validate' => false));
						return $this->redirect('/dashboard');
		        	}
		        }	
		        $this->Session->setFlash('Incorrect Username Or Password!','default',array('class' => 'formError'));
		    }
		}

		public function admin_add() {
			$this->layout = 'admin';
			$this->set('groups',$this->User->Group->find('list'));
			if($this->request->is('post')) {
				if($this->User->save($this->request->data,array('validate' => false))){
					$this->Session->setFlash('User Added!','default',array('class' => 'formSuccess'));
					$this->redirect(array('action' => 'admin_index'));
				}else{
					$errors = $this->User->validationErrors;
				}
			}
		}

		public function admin_delete($id){
			if ($this->User->delete($id)) {
            	$this->Session->setFlash('User Deleted','default',array('class' => 'formError'));
            	$this->redirect(array('action' => 'admin_index'));
        	}
		}

		public function admin_index($approved = null){
			$this->layout = 'admin';
			$this->set('users',$this->User->find('all'));
		}

		public function logout() {
			$this->Session->setFlash('GoodBye!','default',array('class' => 'formError'));
			$this->redirect($this->Auth->logout());
		}



	}
?>