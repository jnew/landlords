<section class="adminSection">
<div class="row">
	<div class="large-12 columns">
	<h1 class="adminSectionHeading adminPageTitle"><?php echo $title;?> Users</h1>
	<?php echo $this->Session->flash();?>
	
	<?php 
			echo $this->Html->link('Add New User',array('controller' => 'users','action' => 'add'),array('class' => 'fadeButton greenAdminButton adminButton'));
			echo $this->Html->link('View None Approved Users',array('controller' => 'users','action' => 'index','none_approved'),array('class' => 'fadeButton blueAdminButton adminButton'));
	 		echo $this->Html->link('View Approved Users',array('controller' => 'users','action' => 'index','approved'),array('class' => 'fadeButton redAdminButton adminButton'));
	 		echo $this->Html->link('View Interested Users',array('controller' => 'users','action' => 'interested'),array('class' => 'fadeButton orangeAdminButton adminButton'));
	?>
	<table class="adminTable postsTable">
		<thead>
			<tr class="blackGradient">
			  <th class="blogTitleCell">Name</th>
			  <th class="blogDatePostedCell">Email</th>
			  <th>Company</th>
			  <th>Interest Location</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($users as $user):?>
				<tr>
				  <td class="adminTitleCell"><?php echo ucfirst($user['Enquiry']['Name']);?></td>
				  <td class="adminDateCell adminEmail"><a href="mailto:<?php echo $user['Enquiry']['Email'];?>"><?php echo $user['Enquiry']['Email'];?></a></td>
				  <td><?php echo $user['Enquiry']['Company'];?></td>
				  <td><?php echo ucfirst($user['Enquiry']['Location']);?></td>
				</tr>
		<?php endforeach;?>
		</tbody>
	</table>
	<?php echo $this->Html->link('Export CSV <i class="fa fa-file"></i>',array('controller' => 'users','action' => 'generateCSV',$title),array('class' => 'fadeButton redAdminButton adminButton','escape' => false));?>
	</div>
</div>
</section>