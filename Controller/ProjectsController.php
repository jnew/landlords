<?php
	class ProjectsController extends AppController {
		public function beforeFilter() {
		    parent::beforeFilter();
		}
		
		public function admin_index(){
			$this->layout = 'admin';
			$projects = $this->Project->find('all',array('group' => 'Project.landlord_id'));
			$i = 0;
			foreach($projects as $project){
				$count = $this->Project->projectCount($project['Project']['landlord_id']);
				$projects[$i]['Project']['count'] = $count;
				$i++;
			}
			$this->set('projects',$projects);
			
		}

		public function admin_add($landlordGroupId = null){
			$this->layout = 'admin';
			$this->loadModel('Landlord');
			$this->set('groups',$this->Landlord->find('list'));
			if(!empty($landlordGroupId)){
				$this->set('landlord',$this->Project->findById($landlordGroupId));
			}
			
			if($this->request->is('post')){
				if(!empty($this->request->data['Project']['year']['year'])){
					$year = $this->request->data['Project']['year']['year'];
					$this->request->data['Project']['year'] = $year;
				}
				
				if($this->Project->save($this->request->data)){
					if(!empty($landlordGroupId)){
						$this->redirect(array('action' => 'admin_landlord',$landlordGroupId));
					}else{
						$this->redirect(array('action' => 'admin_index'));
					}
					
					$this->Session->setFlash('Project Added','default',array('class' => 'formSuccess'));
		   
				}
			}
		}

		public function admin_edit($projectID){
			$this->layout = 'admin';
			$project = $this->Project->findById($projectID);
			$this->set('landlord',$project);
			if($this->request->is('put')){
				if(!empty($this->request->data['Project']['year']['year'])){
					$year = $this->request->data['Project']['year']['year'];
					$this->request->data['Project']['year'] = $year;
				}
				
				if($this->Project->save($this->request->data)){
					$this->Session->setFlash('Project Updated!','default',array('class' => 'formSuccess'));
					$this->redirect(array('action' => 'admin_landlord',$project['Landlord']['landlord_group_id']));
				
		   
				}
			}else{
				$this->request->data = $this->Project->findById($projectID);
			}
		}

		public function admin_delete($id){
			if ($this->Project->delete($id)) {
            	$this->Session->setFlash('Project Deleted','default',array('class' => 'formError'));
            	$this->redirect(array('action' => 'admin_index'));
        	}	
		}

		//Set landlord information and projects linked to that landlord using the landlordId
		public function admin_landlord($landlordId){
			$this->layout = 'admin';
			$this->set('landlord',$this->Project->Landlord->findById($landlordId));
			$this->set('projects',$this->Project->find('all',array(
																    'conditions' => array(
																    	'Project.landlord_id' => $landlordId
																     ),
																     'order' => array(
																     	'Project.year' => 'desc',
																     	'Project.quarter' => 'desc'
																     )	
																  )
													   )
			);
		}


		//Get admin stats and set years for the view
		public function admin_stats($landlordID){
			$this->layout = 'admin';
			$years = $this->Project->getYears($landlordID);
			$this->set('years',$years);
			$this->set('landlord',$this->Project->find('first',array('conditions' => array('landlord_id' => $landlordID))));
		}

		//Ajax method for the quarterly data called within the dashboard of a landlord
		public function admin_quarterlyData($landlordID,$year){
			$this->layout = false;
			$this->autoRender = false;
			$data = $this->Project->constructQuarterlyData($landlordID,$year);
			return $data;
		}

		//Ajax method for the loss data called within the dashboard
		public function admin_lossData($landlordID){
			$this->layout = false;
			$this->autoRender = false;
			$data = $this->Project->constructLossData($landlordID);
			return $data;
		}

		//Ajax method for the year summary data called within the dashboard
		public function admin_yearlySummaryData(){
			$this->layout = false;
			$this->autoRender = false;
			$data = $this->Project->constructYearlySummaryData();
			return $data;
		}

		public function admin_performersData(){
			$this->layout = false;
			$this->autoRender = false;
			$data = $this->Project->constructPerformerData();
			return $data;
		}
	}
?>
