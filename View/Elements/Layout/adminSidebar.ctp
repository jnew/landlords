<div class="adminSidebar">
	<div class="adminProfile">
		<h2><?php echo $this->Session->read('Auth.User.username');?></h2>
		<h5>Role: <?php echo Inflector::singularize($this->Session->read('Auth.User.Group.name'));?></h5>
	</div>
	<nav class="adminNav">
		<ul>
			<li class="adminHasSub">
				<a href="<?php echo $this->webroot;?>dashboard">
					<i class="fa fa-bar-chart"></i> Dashboard
				</a>
			</li>
			<li class="adminHasSub">
				<a><i class="fa fa-file-o"></i> Projects</a>
				<ul class="subAdminMenu">
					<li><?php echo $this->Html->link('<i class="fa fa-list"></i> View Projects',array('admin' => true,'controller' => 'projects','action' => 'admin_index','plugin' => false),array('escape' => false));?></li>
					<li><?php echo $this->Html->link('<i class="fa fa-plus"></i> Add Project',array('admin' => true,'controller' => 'projects','action' => 'add','plugin' => false),array('escape' => false));?></li>
				</ul>
			</li>
			<li class="adminHasSub">
				<a><i class="fa fa-building"></i> Landlords</a>
				<ul class="subAdminMenu">
					<li><?php echo $this->Html->link('<i class="fa fa-list"></i> View Landlords',array('admin' => true,'controller' => 'landlords','action' => 'admin_index','plugin' => false),array('escape' => false));?></li>
					<li><?php echo $this->Html->link('<i class="fa fa-plus"></i> Add Landlord',array('admin' => true,'controller' => 'landlords','action' => 'add','plugin' => false),array('escape' => false));?></li>
				</ul>
			</li>
			<li class="adminHasSub">
				<a><i class="fa fa-users"></i> Landlord Groups</a>
				<ul class="subAdminMenu">
					<li><?php echo $this->Html->link('<i class="fa fa-list"></i> View Landlord Groups',array('admin' => true,'controller' => 'landlord_groups','action' => 'admin_index','plugin' => false),array('escape' => false));?></li>
					<li><?php echo $this->Html->link('<i class="fa fa-plus"></i> Add Group',array('admin' => true,'controller' => 'landlord_groups','action' => 'add','plugin' => false),array('escape' => false));?></li>
				</ul>
			</li>
			<li class="adminHasSub">
				<a><i class="fa fa-users"></i> Users</a>
				<ul class="subAdminMenu">
					<li><?php echo $this->Html->link('<i class="fa fa-list"></i> View Users',array('admin' => true,'controller' => 'users','action' => 'admin_index','plugin' => false),array('escape' => false));?></li>
					<li><?php echo $this->Html->link('<i class="fa fa-plus"></i> Add User',array('admin' => true,'controller' => 'users','action' => 'add','plugin' => false),array('escape' => false));?></li>
				</ul>
			</li>
			
			<li><?php echo $this->Html->link('<i class="fa fa-sign-out"></i> Log Out',array('admin' => true,'controller' => 'users','action' => 'logout','admin' => false,'plugin' => false),array('escape' => false));?></li>
		</ul>
	</nav>
</div>