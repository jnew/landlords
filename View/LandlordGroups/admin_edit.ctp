<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle">Edit Landlord Group <?php echo $landlord['Landlord']['title'];?></h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-8 columns">
					<?php
						echo $this->Form->create('LandlordGroup', array('action' => 'add'));
						echo $this->Form->input('LandlordGroup.id' ,array('type' => 'hidden'));
						echo $this->Form->input('title',array('type' => 'text'));
						echo $this->Form->submit('Save',array('div' => false,'class' => 'fadeButton borderRadiusButton greenAdminButton adminButton'));
						echo $this->Form->End();
					?>
			</div>
		</div>
	</section>
</div>