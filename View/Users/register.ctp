<section class="loginArea registerPage">
	<?php echo $this->Html->image('logo.png', array('class' => 'loginLogo'));?>
	<h1>Register</h1>

	<div class="row">
		<div class="large-12 columns text-center">
			<?php echo $this->Session->flash();?>
		</div>
	</div>

	<div class="sawtooth">
	</div>

	<div class="redrow darkerRow loginFormArea">
		<div class="row">
			<div class="large-10 large-centered columns">
				<p>If you would like to join our network please fill in the details below and a member of our team will contact you.</p>
			</div>
		</div>
	</div>

	<div class="sawtooth saw3">
	</div>

	<div class="row">
		<div class="large-12 columns regFormArea">
			<?php echo $this->Form->create('User',array('controller' => 'users','action' => 'register', 'autocomplete' => 'off'));?>
			<input type="text" style="display:none">
			<input type="password" style="display:none">
			<?php echo $this->Form->input('username',array('type' => 'text','required' => false, 'label' => false, 'placeholder' => 'Choose your username', 'autocomplete' => 'off'));?>
			<?php echo $this->Form->input('first_name',array('type' => 'text','required' => false, 'label' => false, 'placeholder' => 'Your first name'));?>
			<?php echo $this->Form->input('last_name',array('type' => 'text','required' => false, 'label' => false, 'placeholder' => 'Your last name'));?>
			<?php echo $this->Form->input('company',array('type' => 'text','required' => false, 'label' => false, 'placeholder' => 'Company Name'));?>
			<?php echo $this->Form->input('email',array('required' => false, 'label' => false, 'placeholder' => 'Your email', 'autocomplete' => 'off'));?>
			<?php echo $this->Form->input('phone',array('required' => false, 'label' => false, 'placeholder' => 'Your phone'));?>
			<?php echo $this->Form->input('password',array('type' => 'text','required' => false, 'label' => false, 'placeholder' => 'Choose your password', 'autocomplete' => 'off', 'id' => 'passwordField'));?>
			<?php echo $this->Form->input('group_interest',array('options' => $eventGroups,'label' => 'Group Interested In','placeholder' => 'Group Interested In'));?>
			<?php echo $this->Form->submit('Submit',array('div' => false,'class' => 'redButton'));?>
			<?php echo $this->Form->end();?>	
			<p>Please inform us in advance about any disabilities, need for assistance, food allergies or special dietary needs.</p>
		</div>
	</div>


</section>

<div class="sawtooth">
</div>