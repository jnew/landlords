<?php

	$editMarkup = "<span class='adminIcon deleteIcon fa-stack'>";
	$editMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
	$editMarkup .= "<i class='has-tip tip-bottom fa fa-pencil fa-stack-1x' data-tooltip title='Edit $editItem'></i>";
	$editMarkup .= "</span>";

	echo $this->Html->link(
		$editMarkup,
		array('controller' => $controllerName,'action' => 'edit' , $id),
		array('escape' => false)
	);
?>