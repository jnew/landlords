<?php
	class Project extends AppModel {
	    public $actsAs = array('Acl' => array('type' => 'requester'));
	    public $belongsTo = array('Landlord');
	    public function parentNode() {
	        return null;
	    }

	    public $validate = array(
	        'estimated_cost' => array(
	            'notEmpty' => array(
	                'rule' => 'notEmpty',
	                'message' => 'Estimated Cost Required!',
	                'required' => true
	            )
	        )
	        
	    );

	    public function projectCount($landlordID){
	    	$count = $this->find('all',array('conditions' => array('landlord_id' => $landlordID)));
	    	$count = count($count);
	    	return $count;
	    }

		//Returns count of projects over estimated cost
	    public function projectsOverEstimate(){
	    	return $this->find('count',array('conditions' => array('Project.actual_cost > Project.estimated_cost')));
	    }

		//Returns count of projects under estimated
	    public function projectsUnderEstimate(){
	    	return $this->find('count',array('conditions' => array('Project.estimated_cost > Project.actual_cost')));
	    }

	    public function getProfitForLandlord($landlordID){
	    	$actual =  $this->find('all', array(
	    		'conditions' => array('landlord_id' => $landlordID,'Project.estimated_cost > Project.actual_cost'),
	    		'fields' => array('SUM(Project.actual_cost*1000) AS total')
	    		)
	    	);
	    	$estimated =  $this->find('all', array(
	    		'conditions' => array('landlord_id' => $landlordID,'Project.estimated_cost > Project.actual_cost'),
	    		'fields' => array('SUM(Project.estimated_cost*1000) AS total')
	    		)
	    	);
	    	$actual =  $actual[0][0]['total'];
	    	$estimated = $estimated[0][0]['total'];
	    	$profit = $estimated - $actual;
	    	return $profit;
	    }

		//Returns total actual cost
	    public function totalActualCost(){	
	    	$cost = $this->find('all', array(
	    		'fields' => array('SUM(Project.actual_cost*1000) AS total')
	    		)
	    	);
	    	return $cost[0][0]['total'];

	    }

	    //Returns count of landlords
	    public function totalLandlords(){
	    	return $this->find('count',array('fields' => 'DISTINCT Project.landlord_id'));
	    }

	   

	    

	    //Return cost for any given year , accepts type of cost
	    public function totalSummaryCost($type,$year){
	    	$costFields =  $this->find('all',array(
	    			'conditions' => array(
	    				'year' => $year
	    			),
	    			'fields' => array(
					    'SUM('.$type.' * 1000) AS total'
					)
	    		)
	    	);
	    	
	    	return $costFields[0][0]['total'];

	    }
	    //Get total loss for anylandlord and any year
	    public function getLoss($landlordID,$year){
	    	$lossFields =  $this->find('all',array(
	    			'conditions' => array(
	    				'year' => $year,
	    				'landlord_id' => $landlordID,
	    				'actual_cost > estimated_cost'
	    			),
	    			'fields' => array(
					    'SUM(actual_cost * 1000) AS total_actual',
					    'SUM(estimated_cost * 1000) AS total_estimated'
					)
	    		)
	    	);
	    	return $lossFields;
	    }

	    //Calculate loss by subtracting the two values
	    public function calculateLoss($actual,$estimate){
	    	return $actual - $estimate;
	    }

	    public function getCost($landlordID,$type,$year){
	    	$list = $this->find('list',array(
	    		'fields' => $type,
	    		'conditions' => array(
	    			'landlord_id' => $landlordID,
	    			'year' => $year
	    		),
	    		'order' => array(
						     	  'Project.year' => 'asc',
						     	  'Project.quarter' => 'asc'
						     )	
	    		)
	    	);
	    	$list = array_values($list);
	    	return $list;
	    }

	    //Calculate average cost for dashboard
	    public function averageCost(){
	    	$averages =  $this->find('all',array(		
	    			'fields' => array(
					    'AVG(actual_cost * 1000) AS average',
					    
					)
	    		)
	    	);
	    	return ceil($averages[0][0]['average']);
	    }

	    //Find count of projects without an actual cost set for the dashboard
	    public function emptyActualCosts(){
	    	return $this->find('count',array(
	    										'conditions' => array(
	    											'Project.actual_cost' => NULL
	    					   					)
	    					 )
	    	);
	    }

	    //Get list of years that the database has projects for
	    public function getYears($landlordID = null){
	    	if(empty($landlordID)){
	    		$years = $this->find('list',array(
						'group' => array('Project.year'),
						'fields' => array('Project.year')
					)
				);
	    	}else{
		    	$years = $this->find('list',array(
						'conditions' => array('landlord_id' => $landlordID),
						'group' => array('Project.year'),
						'fields' => array('Project.year')
					)
				);
	    	}

			$years = array_values($years);
			return $years;
	    }

	    public function getLandlordList(){
	    	return $this->find('list',array(
	    									'group' => array('Project.landlord_id'),
	    									'fields' => array('Project.landlord_id')
	    									)
	    	);
	    
	    }

	     //Construct data for quarterly data for a given landlord and year
	    public function constructQuarterlyData($landlordID,$year){
	    	$data = array();
	    	$data['labels'] = array('Quarter 1',' Quarter 2','Quarter 3','Quarter 4');	    	
	    	$data['datasets'][0] = array(
	    		'label' => 'Estimated Cost',
	    		'fillColor' => 'rgba(151,187,205,1)',
	    		'strokeColor' => 'rgba(151,187,205,0.8)',
	    		'highlightFill' => 'rgba(151,187,205,0.75)',
		        'highlightStroke' => 'rgba(151,187,205,1)',
		        'data' => array()
	    	);
	    	$data['datasets'][1] = array(
	    		'label' => 'Actual Cost',
	    		'fillColor' => 'rgba(220,220,220,1)',
	    		'strokeColor' => 'rgba(220,220,220,0.8)',
	    		'highlightFill' => 'rgba(220,220,220,0.75)',
		        'highlightStroke' => 'rgba(220,220,220,1)',
		        'data' => array()
	    	);
	    	$actual = $this->getCost($landlordID,'actual_cost',$year);
	    	$estimated = $this->getCost($landlordID,'estimated_cost',$year);

	    	$data['datasets'][0]['data'] = $estimated;
	    	$data['datasets'][1]['data'] = $actual;
	    	return json_encode($data,true);
	    }

	    //Construct chart data for a landlord and show the total loss they have made
	    public function constructLossData($landlordID){
	    	$data = array();
	    	$years = $this->getYears($landlordID);
	    	$data['labels'] =  $years;  	
	    	$data['datasets'][0] = array(
	    	'label' => 'Profit',
			'fillColor' => 'rgba(220,220,220,1)',
            'strokeColor' => 'rgba(220,220,220,1)',
            'pointColor' => 'rgba(151,187,205,1)',
            'pointStrokeColor' => '#fff',
            'pointHighlightFill' => '#fff',
            'pointHighlightStroke' => 'rgba(220,220,220,1)',
		    'data' => array()
	    	);
	    	
	    	$losses = array();
	    	foreach($years as $year){
	    		$loss = $this->getLoss($landlordID,$year);
	    		$lossTotal = $this->calculateLoss($loss[0][0]['total_actual'],$loss[0][0]['total_estimated']);
	    		array_push($losses,$lossTotal);
	    	}
	    	$data['datasets'][0]['data'] = array_values($losses);
	    	return json_encode($data,true);
	    }

	    public function constructPerformerData(){
	    	$data = array();

			$landlords = $this->getLandlordList();
			$i = 0;
			foreach($landlords as $landlordID){
				$landlord = $this->find('first',array('conditions' => array('landlord_id' => $landlordID)));
				$landlordTitle = $landlord['Landlord']['title'];
				$data[$i]['label'] = $landlordTitle;
				//Generate random color here
				$data[$i]['color'] = '#'.dechex(rand(0x000000, 0xFFFFFF));
				//Calculate profit
				$profit = $this->getProfitForLandlord($landlordID);
				$data[$i]['value'] = $profit;
				$i++;
			}
	    	return json_encode($data,true);
	    }

	    //Construct data for chart for a year summary of estimated vs actual cost
	    public function constructYearlySummaryData(){
	    	$data = array();
	    	$years = $this->getYears();
	    	$data['labels'] =  $years;  	
	    	$data['datasets'][0] = array(
	       	'label' => 'Estimated',
			'fillColor' => 'rgba(220,220,220,1)',
	    	'strokeColor' => 'rgba(220,220,220,0.9)',
            'pointColor' => 'rgba(220,220,220,1)',
            'pointStrokeColor' => '#fff',
            'pointHighlightFill' => '#fff',
            'pointHighlightStroke' => 'rgba(220,220,220,1)',
		    'data' => array()
	    	);
	    	$data['datasets'][1] = array(
	    	'label' => 'Estimated Cost',
    		'fillColor' => 'rgba(151,187,205,1)',
    		'strokeColor' => 'rgba(151,187,205,0.8)',
    		'highlightFill' => 'rgba(151,187,205,0.75)',
	        'highlightStroke' => 'rgba(151,187,205,1)',
		    'data' => array()
	    	);
	    	$i = 0;
	    	foreach($years as $year){
	    		$actual = $this->totalSummaryCost('actual_cost',$year);
	    		$estimated = $this->totalSummaryCost('estimated_cost',$year);
	    		$data['datasets'][0]['data'][$i] = $actual;
	    		$data['datasets'][1]['data'][$i] = $estimated;
	    		$i++;
	    	}
	    	return json_encode($data,true);
	    }


	}
?>