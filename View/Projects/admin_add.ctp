<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle">Add Project 
					<?php if(!empty($landlord)):?>
						To <?php echo $landlord['Landlord']['title'];?>
					<?php endif;?>
				</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<?php echo $this->Form->create('Project');?>
		<div class="row">
			<div class="large-6 columns">
				<?php
					if(!empty($landlord)){
						echo $this->Form->input('landlord_id',array('type' => 'hidden','value' => $landlord['Landlord']['id']));
					}
					
					$quarters = array('1' => '1','2' => '2','3' => '3','4' => '4');
					echo $this->Form->input('quarter',array('type' => 'select','options' => $quarters));
					echo $this->Form->input('estimated_cost',array('label' => 'Estimated Cost (Thousands)'));	
				?>
			</div>
			<div class="large-6 columns">
				<?php echo $this->Form->input('year', array('type' => 'date','dateFormat' => 'Y','minYear' => '2000'));?>
				<?php echo $this->Form->input('actual_cost',array('label' => 'Actual Cost (Thousands)'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<?php echo $this->Form->input('landlord_id', array('options' => $groups));?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<?php echo $this->Form->submit('Save',array('div' => false,'class' => 'fadeButton borderRadiusButton greenAdminButton adminButton'));?>
			</div>
		</div>
		<?php echo $this->Form->End();?>
	</section>
</div>