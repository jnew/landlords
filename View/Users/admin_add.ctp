<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle">Add User</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-8 columns">
					<?php
						echo $this->Form->create('User', array('action' => 'add'));
						echo $this->Form->input('username');
						echo $this->Form->input('password');
						echo $this->Form->input('email');
						echo $this->Form->input('group_id', array('options' => $groups));
						echo $this->Form->submit('Save',array('div' => false,'class' => 'fadeButton borderRadiusButton greenAdminButton adminButton'));
						echo $this->Form->End();
					?>

			</div>
		</div>
	</section>
</div>