<?php
	class LandlordGroupsController extends AppController {
		public function beforeFilter() {
		    parent::beforeFilter();
		}
		
		public function admin_index(){
			$this->layout = 'admin';
			$this->set('landlordGroups',$this->LandlordGroup->find('all'));
		}

		public function admin_add(){
			$this->layout = 'admin';
			if($this->LandlordGroup->save($this->request->data)){
				$this->Session->setFlash('Landlord Group Added!','default',array('class' => 'formSuccess'));
				return $this->redirect(array('action' => 'admin_index'));
			}else{
				$errors = $this->LandlordGroup->validationErrors;
			}
		}

		public function admin_edit($id){
			if($this->LandlordGroup->save($this->request->data)){
				$this->Session->setFlash('Landlord Group Updated!','default',array('class' => 'formSuccess'));
				return $this->redirect(array('action' => 'admin_index'));
			}else{
				$errors = $this->LandlordGroup->validationErrors;
			}
		}

		public function admin_delete($id){
			if ($this->LandlordGroup->delete($id)) {
            	$this->Session->setFlash('Group Deleted','default',array('class' => 'formError'));
            	$this->redirect(array('action' => 'admin_index'));
        	}
		}
	}
?>
