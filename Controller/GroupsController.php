<?php
	class GroupsController extends AppController {
		public function beforeFilter() {
		    parent::beforeFilter();
		}
		
		public function admin_add() {
			$this->layout = 'admin';
			if($this->request->is('post')) {
				$this->Group->save($this->request->data);
			}
		}
	}
?>
