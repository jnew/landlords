<?php
	function ordinalSuffix($n){
	  return date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
	}
?>
<?php echo $this->Html->image('write.png', array('class' => 'pageLogo')); ?>
<h1>Your Profile</h1>
<div class="row">
	<div class="large-12 columns">
		<?php echo $this->Session->flash();?>
	</div>
</div>

<div class="sawtooth">
</div>

<div class="memberHeader">
	<div class="row">
		<div class="large-3 columns imgArea">
			<?php if(!empty($user['User']['picture'])):?>
			<?php echo $this->Html->image('uploads/'.$user['User']['picture'],array('class' => 'profileThumbnail'));?>
			<?php else:?>
			<?php endif;?>
		</div>
		<div class="large-6 columns nameArea">
			<h2>Hello!</h2>
			<h1><?php echo $user['User']['first_name'];?></h1>
			<h1><?php echo $user['User']['last_name']; ?></h1>
			<a href="<?php echo $this->webroot;?>users/edit_profile" class="redGhostButton">Edit Profile</a>
			<a  class="redGhostButton">Last Login: <?php echo $this->Time->niceShort($user['User']['last_login']); ?></a>
		</div>
		<div class="large-3 columns">
			&nbsp;
		</div>
	</div>
</div>

<div class="sawtooth saw3">
</div>

<div class="userProfilePage ">
	<?php if(empty($userID)):?>
		<h1>Please Login!</h1>
		<a class="loginLink" href="<?php echo $this->webroot;?>users/login">Login</a> OR <a class="loginLink" href="<?php echo $this->webroot;?>users/register">Register</a>
	<?php elseif($approved == false):?>
		<h1>Your account has not yet been approved!</h1>
	<?php else:?>
		<div class="row">
			<div class="large-12 columns">
				<div class="registeredEvents">
					<?php if(empty($registeredEvents)):?>
						<h2 class="eventPin">You are not booked on to any events!</h2>
					<?php else:?>
						<h2 class="eventPin">Your Upcoming Booked Events</h2>
							<?php $i = 0;?>
							<?php $eventCount = count($registeredEvents); ?> 
							<?php foreach($registeredEvents as $registeredEvent): 
								$year = substr($registeredEvent['Event']['start'],0,4);
								$month = substr($registeredEvent['Event']['start'],5,2);
								$day = substr($registeredEvent['Event']['start'],8,2); ?>
								<?php if($i < 5):?>
								<div class="row subEventRow">
									<div class="large-12 columns">
										<ul class="medium-block-grid-3 upcomingEvents">
											<li>
												<i class="fa fa-chevron-circle-right"></i> <a href="<?php echo $this->webroot;?>events_calendar/events/view/<?php echo $registeredEvent['Event']['id'];?>"><?php echo $registeredEvent['Event']['title'];?></a>
											</li>
											<li>
												<i class="fa fa-clock-o"></i> <?php echo $day; echo ordinalSuffix($day);?> <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?> <?php echo substr($registeredEvent['Event']['start'],11,5); ?> - <?php echo substr($registeredEvent['Event']['end'],11,5); ?>
											</li>
											<li>
												<a class="directions" target="_blank" href="<?php echo $registeredEvent['Event']['directions'];?>"><i class="fa fa-map-marker"></i> GET DIRECTIONS</a> 
												<a href="<?php echo $this->webroot;?>events_calendar/event_registrations/unregister/<?php echo $registeredEvent['Event']['id'];?>"><i class="fa fa-multiple"></i>Unregister <i class="fa fa-times-circle"></i></a> 
											</li>
										</ul>
									</div>
								</div>
								<?php $i++;?>
								<?php endif;?>
							<?php endforeach;?>
							<?php if($eventCount > 5):?>
								<?php echo $this->Html->link('View All My Events',array('controller' => 'users','action' => 'events'), array('class' => 'redButton viewAllEv'));?>
							<?php endif;?>
					<?php endif;?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="registeredEvents">
					<?php if(empty($groupEvents)):?>
						<h2 class="eventPin">You havent got any upcoming group events!</h2>
					<?php else:?>
						<h2 class="eventPin">Upcoming Group Events</h2>
							<?php $i = 0;?>
							<?php $eventCount = count($groupEvents); ?> 
							<?php foreach($groupEvents as $groupEvent): 
								$year = substr($groupEvent['Event']['start'],0,4);
								$month = substr($groupEvent['Event']['start'],5,2);
								$day = substr($groupEvent['Event']['start'],8,2); ?>
								<?php if($i < 10):?>
								<div class="row subEventRow">
									<div class="large-12 columns">
										<ul class="medium-block-grid-3 upcomingEvents">
											<li>
												<i class="fa fa-chevron-circle-right"></i> <a href="<?php echo $this->webroot;?>events_calendar/events/view/<?php echo $groupEvent['Event']['id'];?>"><?php echo $groupEvent['Event']['title'];?></a>
											</li>
											<li>
												<i class="fa fa-clock-o"></i> <?php echo $day; echo ordinalSuffix($day);?> <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?> <?php echo substr($groupEvent['Event']['start'],11,5); ?> - <?php echo substr($groupEvent['Event']['end'],11,5); ?>
											</li>
											<li>
												<a class="directions" target="_blank" href="<?php echo $groupEvent['Event']['directions'];?>"><i class="fa fa-map-marker"></i> GET DIRECTIONS</a> 
												<?php echo $this->Html->link('Quick Book','',array('class'=>'quickBookLinkProfile','data-user' => $user['User']['id'],'data-event' => $groupEvent['Event']['id']));?>
											</li>
										</ul>
									</div>
								</div>
								<?php $i++;?>
								<?php endif;?>
							<?php endforeach;?>
							
					<?php endif;?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="alerts">
					<h2 class="messageIcon">Conversations and Alerts</h2>
					<?php foreach($alerts as $alert):?>
						<div class="row subEventRow formError">
							<div class="large-12 columns">
								<a href="" class="closeDiv closeAlert"  data-alert-id="<?php echo $alert['Alert']['id'];?>"><i class="fa fa-times-circle"></i></a>
								<div class="alert alertPreview" data-alert-title="<?php echo $alert['Alert']['title'];?>" data-full-alert="<?php echo $alert['Alert']['content'];?>">
									<h3 class="alertTitle"><i class="fa fa-exclamation-circle"></i> <?php echo $alert['Alert']['title'];?></h3>
									<p><i class="fa fa-caret-right"></i>
										<?php echo strip_tags($this->Text->truncate($alert['Alert']['content'],150,array('ellipsis' => '...','exact' => false)));?>
									</p>
								</div>
							</div>
						</div>
					<?php endforeach;?>
					<?php if(!empty($recentConversations)):?>
						<?php foreach($recentConversations as $recentConversation):?>
							<div class="row subEventRow">
								<div class="large-12 columns">
									<div class="alert">
										<h3>
											<i class="fa fa-envelope"></i> 
												<?php echo $recentConversation['Conversation']['between'];?>:
												<?php echo ucfirst($recentConversation['Conversation']['first_name']);?> <?php echo ucfirst($recentConversation['Conversation']['last_name']);?>


										</h3>
										<p><i class="fa fa-caret-right"></i> <?php echo $this->Time->niceShort($recentConversation['Conversation']['modified']);?> <a href="<?php echo $this->webroot;?>conversations/conversation/<?php echo $recentConversation['Conversation']['id'];?>" class="redButton viewConv">View</a></p>
									</div>
								</div>
							</div>
						<?php endforeach;?>
					<?php endif;?>
				</div>
				<a class="redButton doubleButton" href="<?php echo $this->webroot;?>conversations/add"><i class="fa fa-comment"></i> New Conversation</a>
				<a class="redButton doubleButton" href="<?php echo $this->webroot;?>conversations"><i class="fa fa-comments"></i> View All</a>
			</div>
		</div>		
	<?php endif;?>
</div>
<div class="sawtooth">
</div>



<div id="alertModal" class="reveal-modal" style="" data-reveal>
	  <h4></h4>
	  <div class="alertContents"></div>
	  <a class="close-reveal-modal">&#215;</a>
</div>