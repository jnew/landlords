<?php 
	class SocialShell extends AppShell {
		public $uses = array('Twitter','LinkedInStatus');
	    public function main() {
	       $this->saveTweets();
	       $this->saveStatuses();
	    }

	    public function saveTweets(){
			$this->layout = false;
			$this->autoRender = false;
			$tweets = $this->Twitter->getTwitterFeed();
			foreach ($tweets as $tweet) {
				$twitterID = $tweet->id;
				$lastCreated = $this->Twitter->find('first', array('conditions' => array('Twitter.twitterid' => $twitterID)));
				if(empty($lastCreated)){
					$twitterStatus = $tweet->text;
					$twitterDate = $tweet->created_at;
					if(isset($tweet->entities->media)){
						$twitterImage = $tweet->entities->media[0]->media_url;
					}else{
						$twitterImage = null;
					}
					$twitterDate = date("Y-m-d H:i:s", strtotime($twitterDate));
					$this->Twitter->create();    		
				    $this->Twitter->save(
			    		array(
			    			'status' => $twitterStatus,
			    			'date' => $twitterDate,
			    			'twitterid' => $twitterID,
			    			'image' => $twitterImage
			    		)
				    );
				}else{
					break;
				}
			}

		}

		public function saveStatuses(){
			$this->layout = false;
			$this->autoRender = false;
			$results = $this->LinkedInStatus->get_statuses();
			$results = json_decode($results['linkedin'],true);
			foreach($results['values'] as $status){
				if($status['creator']['firstName'] == 'Ian' && $status['creator']['lastName'] == 'Coupe'){
					$statusCreator = $status['creator']['firstName'].' '.$status['creator']['lastName'];
					$statusCreatorHeadline = $status['creator']['headline'];
					$statusContent = $status['title'];
					$statusID = $status['id'];
					$lastCreated = $this->LinkedInStatus->find('first',array('conditions' => array('status_id' => $statusID)));
					if(empty($lastCreated)){
						$data = array(
							'text' => $statusContent,
							'status_id' => $statusID,
							'headline' => $statusCreatorHeadline,
							'name' => $statusCreator
		 				);
						$this->LinkedInStatus->create();
						$this->LinkedInStatus->save($data);
					}
				}
			}
		}
}
?>