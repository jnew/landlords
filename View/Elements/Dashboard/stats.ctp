<ul class="large-block-grid-4 text-center dashboardStats">
	<li> 
		<div class="statContainer over">
			<div class="icon">
				<i class="fa fa-exclamation"></i>
			</div>
			<div class="stat">
				<strong><?php echo $projectsOver;?></strong>
				<h6>Projects Over Cost</h6>
			</div>
		</div>

	</li>
	<li>
		<div class="statContainer under">
			<div class="icon">
				<i class="fa fa-check"></i>
			</div>
			<div class="stat">
				<strong><?php echo $projectsUnder;?></strong>
				<h6>Projects Under Cost</h6>
			</div>
		</div>
	</li>
	<li>
		<div class="statContainer projectCost">
			<div class="icon">
				<i class="fa fa-gbp"></i>
			</div>
			<div class="stat">
				<strong><?php echo $this->Number->currency($averageProjectCost, 'GBP');?></strong>
				<h6>Average Project Cost</h6>
			</div>
		</div>
	</li>
	<li>
		<div class="statContainer landlordCount">
			<div class="icon">
				<i class="fa fa-building"></i>
			</div>
			<div class="stat">
				<strong><?php echo $totalLandlords;?></strong>
				<h6>Total Number Of Landlords</h6>
			</div>
		</div>
	</li>
</ul>