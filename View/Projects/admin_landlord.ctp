<div class="adminPage">
	<section class="adminSection">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="adminSectionMainHeading adminPageTitle"><?php echo $landlord['Landlord']['title'];?> : Projects</h1>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<?php echo $this->Html->link('Add Project',array('controller' => 'projects','action' => 'add',$landlord['Landlord']['landlord_group_id']),array('class' => 'adminButton greenAdminButton'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<table class="adminTable postsTable">
					<thead>
						<tr class="blackGradient">
						  <th>Year</th>
						  <th>Quarter</th>
						  <th>Estimated Cost</th>
						  <th>Actual Cost</th>
						  <th class="blogActionsCell text-center">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($projects as $project):?>
						<tr>
						  <td class="adminTitleCell">
						 	 <?php echo $project['Project']['year'];?>
						  </td>
						  <td class="adminTitleCell">
						 	 <?php echo $project['Project']['quarter'];?>
						  </td>
						  <td class="adminTitleCell">
						 	 <?php echo $project['Project']['estimated_cost'];?>
						  </td>
						  <td class="adminTitleCell">
						  	<?php if($project['Project']['actual_cost'] > $project['Project']['estimated_cost']):?>
						  		<span class="overPrice">
						  			<?php echo $project['Project']['actual_cost'];?> 
						  		</span>
						  	<?php else:?>
								<?php echo $project['Project']['actual_cost'];?>
						  	<?php endif;?>
						  </td>
						  <td class="adminActionsCell articlesActions text-center">
						  	<?php echo $this->Element('Global/deleteIcon',array('controllerName' => 'projects','id' => $project['Project']['id'],'editItem' => 'project'));?>
						  	<?php echo $this->Element('Global/editIcon',array('controllerName' => 'projects','id' => $project['Project']['id'],'editItem' => 'project'));?>
						  </td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>