$(document).ready(function(){
//Initialize Foundation
	$(document).foundation();
	url = $('.rootURL').text().trim();
	
//Admin menu sidebar
	var menu = $('.left-off-canvas-menu');
	var wrap = $('.inner-wrap');

	$(document).on('resize', function() {
	  menu.height($(this).height());
	  wrap.height($(this).height());
	});

	// Initialize height
	$(document).trigger('resize');


	windowHeight = $(document).height();
	$('.adminSidebar').height(windowHeight+100);

	$('.adminHasSub').click(function(){
		$(this).children().eq(1).slideToggle(500);
	});

//Charts
	if($('.canvas-container').length > 0){
		$('.canvas-container canvas').each(function(){
			landlordID = $(this).attr('data-landlord-id');
			year = $(this).attr('data-year');
			canvasID = $(this).attr('id');
			dataSet = $(this).attr('data-set');
			if(dataSet == 'quarterly'){
				buildQuarterlyChart(landlordID,year,canvasID,dataSet);
			}else if(dataSet == 'loss'){
				buildLossChart(landlordID,canvasID,dataSet)
			}else if(dataSet == 'yearlySummary'){
				buildSummaryYearChart(canvasID,dataSet);
			}else if(dataSet == 'performers'){
				buildPerformerPieChart(canvasID,dataSet);
			}
			
		});
	}


//Quarter data dropdown change
	$('.yearDropdownQuarter').on('change', function() {
	  year = $(this).val();
	  $('.quarterYear').text(year);
	  canvasID = $(this).attr('data-canvas-id');
	  landlordID = $('#'+canvasID).attr('data-landlord-id');
	  dataSet = $('#'+canvasID).attr('data-set');
	  buildQuarterlyChart(landlordID,year,canvasID,dataSet);
	});

//Build quarter data chart
	function buildQuarterlyChart(landlordID,year,canvasID,dataSet){
		canvasContainer = $('#'+canvasID).parent();
		canvasContainer.empty();
		canvasContainer.html('<canvas data-set="quarterly" data-type="Bar" data-landlord-id="'+landlordID+'" id="quarterlyStats" data-year="'+year+'"></canvas>');
		var canvas = document.getElementById(canvasID);
		var ctx = canvas.getContext("2d");
		$.post(url+"admin/projects/"+dataSet+"Data/"+landlordID+'/'+year,function(data) {
			var options = {
				 barShowStroke : true,
				 scaleShowLabels: true,
				 responsive:true,
				 showTooltips:true
			}
		    data = $.parseJSON(data);
		    new Chart(ctx).Bar(data, options);
		});
	}

//Build Profit Chart
	function buildLossChart(landlordID,canvasID){
		canvasLossContainer = $('#'+canvasID).parent();
		canvasLossContainer.empty();
		canvasLossContainer.html('<canvas data-set="loss" data-type="Line" data-landlord-id="'+landlordID+'" id="lossStats"></canvas>');
		var canvas = document.getElementById(canvasID);
		var ctx = canvas.getContext("2d");
		$.post(url+"admin/projects/"+dataSet+"Data/"+landlordID,function(data) {
			var options = {
				 barShowStroke : true,
				 scaleShowLabels: true,
				 responsive:true,
				 showTooltips:true
			}
		    data = $.parseJSON(data);
		    new Chart(ctx).Line(data, options);
		});
	}

	function buildSummaryYearChart(canvasID,dataSet){
		canvasSummaryContainer = $('#'+canvasID).parent();
		canvasSummaryContainer.empty();
		canvasSummaryContainer.html('<canvas data-set="loss" data-type="Line"  id="yearlyStats"></canvas>');
		var canvas = document.getElementById(canvasID);
		var ctx = canvas.getContext("2d");
		$.post(url+"admin/projects/"+dataSet+"Data",function(data) {
			var options = {
				 barShowStroke : true,
				 scaleShowLabels: true,
				 responsive:true,
				 showTooltips:true
			}
		    data = $.parseJSON(data);
		    new Chart(ctx).Bar(data, options);
		});
	}

	function buildPerformerPieChart(canvasID,dataSet){
		canvasPerformerContainer = $('#'+canvasID).parent();
		canvasPerformerContainer.empty();
		canvasPerformerContainer.html('<canvas data-set="loss" data-type="Line"  id="performers"></canvas>');
		var canvas = document.getElementById(canvasID);
		var ctx = canvas.getContext("2d");
		$.post(url+"admin/projects/"+dataSet+"Data",function(data) {
			var options = {
				 barShowStroke : true,
				 scaleShowLabels: true,
				 responsive:true,
				 showTooltips:true
			}
		    data = $.parseJSON(data);
		    new Chart(ctx).Pie(data, options);
		});
	}
});