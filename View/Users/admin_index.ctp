<section class="adminSection">
<div class="row">
	<div class="large-12 columns">
	<h1 class="adminSectionHeading adminPageTitle">Users</h1>
	<?php echo $this->Session->flash();?>
	<?php if(!empty($users)):?>
		<table class="adminTable postsTable">
			<thead>
				<tr class="blackGradient">
				  <th class="blogTitleCell">Name</th>
				  <th class="blogDatePostedCell">Email</th>
				  <th class="blogActionsCell text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user):?>
				<?php if($user['User']['id'] != $this->Session->read('Auth.User.id')):?>
					<tr>
					  <td class="adminTitleCell">
					  <?php echo $user['User']['username'];?>
					  </td>
					  <td class="adminDateCell adminEmail">
					  	<a href="mailto:<?php echo $user['User']['email'];?>">
					  		<?php echo $user['User']['email'];?>
					  	</a>
					  </td>
					  
					  
					  <td class="adminActionsCell articlesActions text-center">
					  	<?php echo $this->Element('Global/deleteIcon',array('controllerName' => 'users','id' => $user['User']['id'],'editItem' => 'user'));?>
					  </td>
					</tr>
				<?php endif;?>

			<?php endforeach;?>
			</tbody>
		</table>
	<?php else:?>
		<h1>No Users Found!</h1>
	<?php endif;?>

	</div>
</div>
</section>