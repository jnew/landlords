<?php
	App::uses('AuthComponent', 'Controller/Component');
	class User extends AppModel {
    public $belongsTo = array('Group');
    public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));
    public $validate = array(
        'username' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Username Required!',
                'required' => true
            ),
            'uniqueUsernameRule' => array(
                'rule' => 'isUnique',
                'message' => 'Username Already Exists!'
            )
        ),
        'password' => array(
            'rule' => 'notEmpty',
            'message' => 'Password Required!',
            'required' => true
        ),
        'email' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Email Required!',
                'required' => true
            ),
            'uniqueEmailRule' => array(
                'rule' => 'isUnique',
                'message' => 'Email Already Exists!'
            )
        ),
        
    );


    public function beforeSave($options = array()) {
        if(!empty($this->data['User']['password'])){
            $this->data['User']['password'] = AuthComponent::password(
              $this->data['User']['password']
            );
            return true; 
        }

    }
    
    public function bindNode($user) {
    	return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

    public function setPasswordFlag($userID) {
        $this->id = $userID;
        $resetKey = uniqid();
        $this->saveField('reset_key',$resetKey);
        return $resetKey;
    }

    public function savePassword($data) {
        $this->id = $data['User']['id'];
        $this->saveField('password', $data['User']['password']);
    }

    public function clearPasswordFlag($userID) {
        $this->id = $userID;
        $this->saveField('reset_key', null);
    }
}

?>