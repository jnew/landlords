<section class="adminSection">
<div class="row">
	<div class="large-12 columns">
	<h1 class="adminSectionHeading adminPageTitle">Landlord Groups</h1>
	<?php echo $this->Session->flash();?>
	<?php if(!empty($landlordGroups)):?>
		<table class="adminTable postsTable">
			<thead>
				<tr class="blackGradient">
				  <th class="blogTitleCell">Name</th>
				  <th class="blogActionsCell text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($landlordGroups as $landlordGroup):?>
				<tr>
				  <td class="adminTitleCell">
				 	 <?php echo $landlordGroup['LandlordGroup']['title'];?>
				  </td>
				  <td class="adminActionsCell articlesActions text-center">
				  	<?php echo $this->Element('Global/deleteIcon',array('controllerName' => 'landlord_groups','id' => $landlordGroup['LandlordGroup']['id'],'editItem' => 'landlord group'));?>
				  </td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	<?php else:?>
		<h1>No Landlords Found!</h1>
	<?php endif;?>

	</div>
</div>
</section>