<?php
	class DashboardsController extends AppController {
		public function beforeFilter() {
		    parent::beforeFilter();   
		}
		
		public function admin_index(){
			$this->loadModel('Project');
			$this->layout = 'admin';
			$this->set('years',$this->Project->getYears());
			$this->set('projectsOver',$this->Project->projectsOverEstimate());
			$this->set('projectsUnder',$this->Project->projectsUnderEstimate());
			$this->set('averageProjectCost',$this->Project->averageCost());
			$this->set('totalLandlords',$this->Project->totalLandlords());
			$this->set('actualUpdate',$this->Project->emptyActualCosts());
		}
		
	}
?>
