<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Landlords Application : <?php echo $this->fetch('title'); ?>
	</title>


	<?php
		if( in_array( $_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
			echo '<script src="//localhost:1990/livereload.js"></script>';
		}
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->Html->css('styles.css');
	?>
	<meta name="viewport" content="width=device-width, user-scalable=no" />
</head>
<body>
<div class="rootURL" style="display:none;"><?php echo $this->Html->url( '/', true );?></div>
	<?php echo $this->fetch('content'); ?>
</body>
<?php echo $this->Element('Global/javascript'); ?>
</html>