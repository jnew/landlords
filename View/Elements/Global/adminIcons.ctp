<?php
	$editMarkup = "<span class='adminIcon editIcon fa-stack'>";
	$editMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
	$editMarkup .= "<i class='has-tip tip-bottom fa fa-pencil fa-stack-1x' data-tooltip title='Edit $editItem'></i>";
	$editMarkup .= "</span>";

	echo $this->Html->link(
		$editMarkup,
		array('controller' => $controllerName,'action' => 'edit' , $id),
		array('escape' => false)
	);

	$deleteMarkup = "<span class='adminIcon deleteIcon fa-stack'>";
	$deleteMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
	$deleteMarkup .= "<i class='has-tip tip-bottom fa fa-times fa-stack-1x' data-tooltip title='Delete $editItem'></i>";
	$deleteMarkup .= "</span>";

	echo $this->Html->link(
		$deleteMarkup,
		array('controller' => $controllerName,'action' => 'delete' , $id),
		array('escape' => false),'Are you sure you want to delete this '.strtolower($editItem).'. This action cannot be undone!'
	);

	$viewMarkup = "<span class='adminIcon viewIcon fa-stack'>";
	$viewMarkup .= "<i class='fa fa-circle fa-stack-2x'></i>";
	$viewMarkup .= "<i class='has-tip tip-bottom fa fa-eye fa-stack-1x' data-tooltip title='View $editItem'></i>";
	$viewMarkup .= "</span>";

	echo $this->Html->link(
		$viewMarkup,
		array('controller' => $controllerName,'action' => 'view' , $id),
		array('escape' => false)
	);

?>